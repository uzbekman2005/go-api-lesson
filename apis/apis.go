package apis

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func HelloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world endpoint hit")
}

var Dsn = "host=localhost port=5432 user=azizbek password=Azizbek dbname=goapi sslmode=disable"

func HadleFuntion() {
	router := mux.NewRouter()
	router.HandleFunc("/", HelloWorld)
	router.HandleFunc("/school/{name}/{location}/{phone}", CreateNewSchool).Methods("POST")
	router.HandleFunc("/school/{id}/{name}/{location}/{phone}", UpdateSchool).Methods("PUT")
	router.HandleFunc("/school/{id}", DeleteSchool).Methods("DELETE")
	router.HandleFunc("/school/{id}", GetSchoolById).Methods("GET")
	router.HandleFunc("/schools", GetAllSchools).Methods("GET")

	// new user := /user/{first_name}/{last_name}/{school_id}/{email}/register
	// update user := /user/{id}/{first_name}/{last_name}/{school_id}/{email}/update
	// delete user := /user/{id}/delete
	// get user := /user/{id}/get
	// get all users := /users

	fmt.Println("Server is listening...")
	if err := http.ListenAndServe("localhost:8081", router); err != nil {
		fmt.Println("Listen and serve error", err)
	}

}
