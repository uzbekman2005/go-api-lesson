package user

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Person struct {
	gorm.Model
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	SchoolID  uint   `json:"school_id"`
	Email     string `json:"email"`
}

type School struct {
	gorm.Model
	Name     string `json:"name"`
	Location string `json:"location"`
	Phone    string `json:"phone"`
	Person   Person `json:"-" gorm:"foreignKey:school_id"`
}

func InitialMigrations() {
	dsn := "host=localhost port=5432 user=azizbek password=Azizbek dbname=goapi sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection", err)
		return
	}
	if err := db.AutoMigrate(&School{}); err != nil {
		fmt.Println("Error while creating schools table", err)
		return
	}
	if err := db.AutoMigrate(&Person{}); err != nil {
		fmt.Println("Error while creating people table", err)
		return
	}
	fmt.Println("Migrations Success")
}

