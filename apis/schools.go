package apis

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/go-api-lesson/user"

	"github.com/gorilla/mux"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func CreateNewSchool(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(Dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection with db", err)
		return
	}
	school := user.School{}
	vars := mux.Vars(r)
	school.Name = vars["name"]
	school.Location = vars["location"]
	school.Phone = vars["phone"]

	if err := db.Create(&school).Error; err != nil {
		fmt.Println("Error while inserting to schools table", err)
		return
	}

	fmt.Fprintf(w, "User successfully created")

}

func UpdateSchool(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(Dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection with db", err)
		return
	}
	school := user.School{}

	vars := mux.Vars(r)
	id := vars["id"]

	if err := db.Where("id = ?", id).Find(&school).Error; err != nil {
		fmt.Fprintf(w, "School not found")
		return
	}

	school.Name = vars["name"]
	school.Phone = vars["phone"]
	school.Location = vars["location"]
	if err := db.Save(&school).Error; err != nil {
		fmt.Fprintf(w, "couldn't update")
		return
	}
	fmt.Fprintf(w, "school info updated successfully ")
}

func DeleteSchool(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(Dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection with db", err)
		return
	}

	school := user.School{}
	vars := mux.Vars(r)
	id := vars["id"]
	if err := db.Where("id = ?", id).Find(&school).Error; err != nil {
		fmt.Fprintf(w, "School not found %s", err)
		return
	}
	if err := db.Delete(&school).Error; err != nil {
		fmt.Fprintf(w, "Couldn't delete %v", err)
		return
	}
	fmt.Fprintf(w, "School succesfully deleted")
}

func GetSchoolById(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(Dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection with db", err)
		return
	}

	school := user.School{}
	id := mux.Vars(r)["id"]
	if err := db.First(&school, "id = ?", id).Error; err != nil {
		fmt.Fprintf(w, "Coudn't found school: %v", err)
	}
	res, err := json.MarshalIndent(school, "", "    ")
	if err != nil {
		fmt.Println("Error while marshaling to json")
		return
	}
	fmt.Fprintf(w, "%s", res)
}

func GetAllSchools(w http.ResponseWriter, r *http.Request) {
	db, err := gorm.Open(postgres.Open(Dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Error while creating connection with db", err)
		return
	}

	schools := []user.School{}
	if err := db.Find(&schools).Error; err != nil {
		fmt.Fprintf(w, "Coudn't found school: %v", err)
	}
	res, err := json.MarshalIndent(schools, "", "    ")
	if err != nil {
		fmt.Println("Error while marshaling to json")
		return
	}
	fmt.Fprintf(w, "%s", res)
}
