package main

import (
	"gitlab.com/go-api-lesson/apis"
	"gitlab.com/go-api-lesson/user"
)

func main() {
	user.InitialMigrations()
	apis.HadleFuntion()
}
